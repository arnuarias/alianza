import { Component } from '@angular/core';
import { Client } from 'src/app/models/client.model';
import { ClientService } from 'src/app/services/client.service';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css'],
})
export class ClientsListComponent {
  clients?: Client[];
  currentClient: Client = {};
  currentIndex = -1;
  sharedKey = '';
  aSharedKey = '';
  aBusinessId = '';
  aEmail = '';
  aPhone = '';
  aStartDate = '';
  aEndDate = '';
  advancedSearchDiv:boolean = false;

  constructor(private clientService: ClientService) {}

  ngOnInit(): void {
    this.retrieveClients();
  }

  retrieveClients(): void {
    this.clientService.getAll().subscribe({
      next: (data) => {
        this.clients = data;
        console.log(data);
      },
      error: (e) => console.error(e)
    });
  }

  refreshList(): void {
    this.retrieveClients();
    this.currentClient = {};
    this.currentIndex = -1;
  }

  setActiveClient(client: Client, index: number): void {
    this.currentClient = client;
    this.currentIndex = index;
  }

  searchSharedKey(): void {
    this.currentClient = {};
    this.currentIndex = -1;

    this.clientService.findBySharedKey(this.sharedKey).subscribe({
      next: (data) => {
        this.clients = data;
        console.log(data);
      },
      error: (e) => console.error(e)
    });
  }

  advancedSearch(): void {
    this.currentClient = {};
    this.currentIndex = -1;

    this.clientService.advancedSearch(this.aSharedKey, this.aBusinessId, this.aEmail, this.aPhone, this.aStartDate, this.aEndDate).subscribe({
      next: (data) => {
        this.clients = data;
        console.log(data);
      },
      error: (e) => console.error(e)
    });
  }

  showAdvancedSearchDiv(){
    this.advancedSearchDiv=!this.advancedSearchDiv;
  }

   closeSearch(): void {
    this.advancedSearchDiv=false;
  }
}
