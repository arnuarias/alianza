import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Client } from 'src/app/models/client.model';
import { ClientService } from 'src/app/services/client.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css'],
})

export class AddClientComponent implements OnInit {
  title = "Add Client"
  client: Client = {
    sharedKey: '',
    businessId: '',
    email: '',
    phone: '',
    startDate: '',
    endDate: ''
  };
  submitted = false;
  clientForm: FormGroup = new FormGroup({
    sharedKey: new FormControl(''),
    businessId: new FormControl(''),
    email: new FormControl(''),
    phone: new FormControl(''),
    startDate: new FormControl(''),
    endDate: new FormControl('')
  });

  constructor(private clientService: ClientService) {}
  private formBuilder = new FormBuilder()

  saveClient(): void {
    if (this.clientForm.invalid) {
      console.log(this.clientForm.invalid)
      for (const key in this.f) {
        const control = this.f[key];
            control.markAsTouched();
       }
       return;
    }
    const data = {
      sharedKey: this.client.sharedKey,
      businessId: this.client.businessId,
      email: this.client.email,
      phone: this.client.phone,
      startDate: this.client.startDate,
      endDate: this.client.endDate
    };

    this.clientService.create(data).subscribe({
      next: (res) => {
        console.log(res);
        this.submitted = true;
      },
      error: (e) => console.error(e)
    });
  }

  newClient(): void {
    this.submitted = false;
    this.client = {
      sharedKey: '',
      businessId: '',
      email: '',
      phone: '',
      startDate: '',
      endDate: ''
    };
  }

  @Output() closeAdd = new EventEmitter();

   closeAddB(): void {
      this.closeAdd.emit();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.clientForm.controls;
  }

  ngOnInit(): void {
    this.clientForm = this.formBuilder.group(
      {
        sharedKey: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
          ]
        ],
        email: ['', [Validators.required, Validators.email]],
        businessId: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
          ]
        ],
        phone: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
          ]
        ],
        startDate: [
          '',
          [
            Validators.required,
            Validators.minLength(10),
            Validators.maxLength(10)
          ]
        ],
        endDate: [
          '',
          [
            Validators.required,
            Validators.minLength(10),
            Validators.maxLength(10)
          ]
        ],
      }
    );
  }

  onSubmit() {

    if (this.clientForm.invalid) {
      return;
    }

  }
}
