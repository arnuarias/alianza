import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../models/client.model';

const baseUrl = 'http://localhost:8080/api/clients';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Client[]> {
    return this.http.get<Client[]>(baseUrl);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  findBySharedKey(sharedKey: any): Observable<Client[]> {
    return this.http.get<Client[]>(`${baseUrl}?sharedKey=${sharedKey}`);
  }

  advancedSearch(sharedKey: any, businessId: any, email: any, phone: any, startDate: any, endDate: any): Observable<Client[]> {
    let params = '';
    if (sharedKey.length > 0){
      params += `sharedKey=${sharedKey}`;
    }else if(businessId.length > 0 && params.length > 0){
      params += `&businessId=${businessId}`;
    }else if(businessId.length > 0){
      params += `businessId=${businessId}`;
    }else if(email.length > 0 && params.length > 0){
      params += `&email=${email}`;
    }else if(email.length > 0){
      params += `email=${email}`;
    }else if(phone.length > 0 && params.length > 0){
      params += `&phone=${phone}`;
    }else if(email.length > 0){
      params += `phone=${phone}`;
    }else if(startDate.length > 0 && params.length > 0){
      params += `&startDate=${startDate}`;
    }else if(email.length > 0){
      params += `startDate=${startDate}`;
    }else if(endDate.length > 0 && params.length > 0){
      params += `&endDate=${endDate}`;
    }else if(email.length > 0){
      params += `endDate=${endDate}`;
    }

    return this.http.get<Client[]>(`${baseUrl}?` + params);
  }

  downloadCSV(): Observable<any> {
    return this.http.get(`${baseUrl}/export`, {
      params: {},
      responseType: "text"
    });
  }
}
