export class Client {
  id?: any;
  sharedKey?: string;
  businessId?: string;
  email?: string;
  phone?: string;
  startDate?: string;
  endDate?: string;

}