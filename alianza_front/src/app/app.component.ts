import { Component, ViewChild } from '@angular/core';
import { ClientService } from 'src/app/services/client.service';
import { ClientsListComponent } from './components/clients-list/clients-list.component';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  newDiv:boolean = false;

  title = 'Alianza';

  constructor(private clientService: ClientService) {}

  @ViewChild(ClientsListComponent) listUsers !:any ;

    showNew(){
        this.newDiv=!this.newDiv;
    }

    closeAdd(){
      this.newDiv=false;
      this.listUsers.refreshList();
  }
  closeSearch(){
    this.newDiv=false;
  }

    exportCSV(): void {
      this.clientService.downloadCSV().subscribe((buffer) => {
        const data: Blob = new Blob([buffer], {
          type: "text/csv;charset=utf-8"
        });
        saveAs(data, "clients.csv");
      });
    }
}
