package com.alianza.alianza.service;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alianza.alianza.helper.CSVHelper;
import com.alianza.alianza.model.Client;
import com.alianza.alianza.repository.ClientRepository;

@Service
public class CSVService {

  @Autowired
  ClientRepository repository;
  
  public ByteArrayInputStream load() {
    List<Client> clients = repository.findAll();

    ByteArrayInputStream in = CSVHelper.clientsToCSV(clients);
    return in;
  }
}
