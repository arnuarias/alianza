package com.alianza.alianza.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;

import com.alianza.alianza.model.Client;
import com.alianza.alianza.repository.ClientRepository;

import com.alianza.alianza.service.CSVService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ClientController {

	@Autowired
	ClientRepository clientRepository;

	@GetMapping("/clients")
	public ResponseEntity<List<Client>> getAllClients(@RequestParam(required = false) String sharedKey,
	@RequestParam(required = false) String businessId,
	@RequestParam(required = false) String email,
	@RequestParam(required = false) String phone,
	@RequestParam(required = false) String startDate,
	@RequestParam(required = false) String endDate
	) {
		try {
			List<Client> clients = new ArrayList<Client>();

			if (sharedKey == null)
				clientRepository.findAll().forEach(clients::add);
			else if (sharedKey != null && businessId == null && email == null && phone == null && startDate == null && endDate == null)
				clientRepository.findBySharedKeyIgnoreCase(sharedKey).forEach(clients::add);
			else
			    clientRepository.findBySharedKeyAndBusinessIdAndEmailAndPhoneAndStartDateAndEndDateContainingIgnoreCase(sharedKey, businessId, email, phone, startDate, endDate).forEach(clients::add);

			if (clients.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(clients, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/clients")
	public ResponseEntity<Client> createClient(@RequestBody Client client) {
		try {
			Client _client = clientRepository
					.save(new Client(client.getSharedKey(), client.getBusinessId(), client.getEmail(), client.getPhone(), client.getStartDate(), client.getEndDate()));
			return new ResponseEntity<>(_client, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	  @Autowired
  CSVService fileService;
  
  @GetMapping("/clients/export")
  public ResponseEntity<Resource> getFile() {
    String filename = "clients.csv";
    InputStreamResource file = new InputStreamResource(fileService.load());

    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
        .contentType(MediaType.parseMediaType("application/csv"))
        .body(file);
  }
}
