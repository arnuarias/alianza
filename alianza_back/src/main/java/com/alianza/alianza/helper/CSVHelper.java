package com.alianza.alianza.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;

import com.alianza.alianza.model.Client;

public class CSVHelper {

  public static ByteArrayInputStream clientsToCSV(List<Client> clients) {
    final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
        CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);) {
          csvPrinter.printRecord("ID", "Shared Key", "Business Id","Email","Phone","Start Date","End Date");
      for (Client client : clients) {
        List<String> data = Arrays.asList(
              String.valueOf(client.getId()),
              client.getSharedKey(),
              client.getBusinessId(),
              client.getEmail(),
              client.getPhone(),
              client.getStartDate(),
              client.getEndDate()
            );

        csvPrinter.printRecord(data);
      }

      csvPrinter.flush();
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
    }
  }
}
