package com.alianza.alianza.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alianza.alianza.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

  List<Client> findBySharedKeyIgnoreCase(String sharedKey);

  List<Client> findBySharedKeyAndBusinessIdAndEmailAndPhoneAndStartDateAndEndDateContainingIgnoreCase(String sharedKey, String businessId, String email, String phone, String startDate, String endDate);
}
