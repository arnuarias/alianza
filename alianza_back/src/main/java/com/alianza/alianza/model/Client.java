package com.alianza.alianza.model;

import jakarta.persistence.*;

@Entity
@Table(name = "clients")
public class Client {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "shared_key")
  private String sharedKey;

  @Column(name = "business_id")
  private String businessId;

  @Column(name = "email")
  private String email;

  @Column(name = "phone")
  private String phone;

  @Column(name = "start_date")
  private String startDate;

  @Column(name = "end_date")
  private String endDate;

  public Client() {

  }

  public Client(String sharedKey, String businessId, String email, String phone, String startDate, String endDate) {
    this.sharedKey = sharedKey;
    this.businessId = businessId;
    this.email = email;
    this.phone = phone;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public long getId() {
    return id;
  }

  public String getSharedKey() {
    return sharedKey;
  }

  public void setSharedKey(String sharedKey) {
    this.sharedKey = sharedKey;
  }

  public String getBusinessId() {
    return businessId;
  }

  public void setBusinessId(String businessId) {
    this.businessId = businessId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  @Override
  public String toString() {
    return "Client [id=" + id + ", sharedKey=" + sharedKey + ", businessId=" + businessId + ", email=" + email + ", phone=" + phone + ", startDate=" + startDate + ", endDate=" + endDate + "]";
  }

}
