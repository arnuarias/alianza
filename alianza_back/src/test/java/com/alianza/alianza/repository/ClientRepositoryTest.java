package com.alianza.alianza.repository;

import com.alianza.alianza.model.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class ClientRepositoryTest {

    @Autowired
    private ClientRepository clientRepository;

    @Test
    void findBySharedKeyIgnoreCase() {
        Client client = new Client("key1", "business1", "email1@email1.com", "1111111111", "2023-11-25", "2023-11-25");
        clientRepository.save(client);

        List<Client> result = clientRepository.findBySharedKeyIgnoreCase("Key1");

        assertEquals(1, result.size());
        assertEquals("key1", result.get(0).getSharedKey());
    }

    @Test
    void findBySharedKeyAndBusinessIdAndEmailAndPhoneAndStartDateAndEndDateIgnoreCase() {
        Client client = new Client("key1", "business1", "email1@email1.com", "1111111111", "2023-11-25", "2023-11-25");
        clientRepository.save(client);

        List<Client> result = clientRepository.findBySharedKeyAndBusinessIdAndEmailAndPhoneAndStartDateAndEndDateContainingIgnoreCase(
                "key1", "business1", "email1@email1.com", "1111111111", "2023-11-25", "2023-11-25"
        );

        assertEquals(1, result.size());
        assertEquals("key1", result.get(0).getSharedKey());
    }
}
