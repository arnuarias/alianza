package com.alianza.alianza.controller;

import com.alianza.alianza.model.Client;
import com.alianza.alianza.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClientRepository clientRepository;

    @Test
    void getAllClients() throws Exception {
        Client client = new Client("key1", "business1", "email1@email1.com", "phone1", "2023-11-25", "2023-11-25");
        clientRepository.saveAll(Arrays.asList(client));

        mockMvc.perform(get("/api/clients").contentType(MediaType.APPLICATION_JSON))
                .andExpect(ResultMatcher.matchAll(
                        jsonPath("$[0].sharedKey").value("key1"),
                        jsonPath("$[0].businessId").value("business1"),
                        jsonPath("$[0].email").value("email1@email1.com"),
                        jsonPath("$[0].phone").value("phone1"),
                        jsonPath("$[0].startDate").value("2023-11-25"),
                        jsonPath("$[0].endDate").value("2023-11-25")
                ));
    }
}
