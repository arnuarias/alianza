package com.alianza.alianza.service;

import com.alianza.alianza.model.Client;
import com.alianza.alianza.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@SpringBootTest
class CSVServiceTest {

    @Autowired
    private CSVService csvService;

    @MockBean
    private ClientRepository clientRepository;

    @Test
    void load() {
        Client client = new Client("key1", "business1", "email1", "phone1", "start1", "end1");
        when(clientRepository.findAll()).thenReturn(Arrays.asList(client));

        ByteArrayInputStream result = csvService.load();

        assertNotNull(result);
    }

    @Test
    void loadEmptyList() {
        when(clientRepository.findAll()).thenReturn(Collections.emptyList());

        ByteArrayInputStream result = csvService.load();

        assertNotNull(result);
    }
}
