# Alianza

Prueba Desarrollador Senior Backend - Alianza

## Requisitos Previos

Asegúrate de tener instalado en tu sistema:

- [Java JDK](https://www.oracle.com/java/technologies/javase-downloads.html)
- [Maven](https://maven.apache.org/download.cgi)

## Configuración del Proyecto

1. Clona este repositorio: `git clone https://github.com/arnuarias/alianza.git`
2. Accede al directorio del proyecto: `cd alianza`

## Ejecución

Para ejecutar el proyecto, ejecuta el siguiente comando desde la raíz del proyecto:

```bash
./mvnw spring-boot:run

## Test

Para ejecutar las pruebas unitarias del proyecto, ejecuta el siguiente comando desde la raíz del proyecto:

```bash
./mvnw test